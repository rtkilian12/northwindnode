'use strict';

// Categories module config
angular.module('categories').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Categories', 'categories', 'dropdown', '/categories(/create)?');
		Menus.addSubMenuItem('topbar', 'categories', 'List categories', 'categories');
		Menus.addSubMenuItem('topbar', 'categories', 'New categories', 'categories/create');
	}
]);
